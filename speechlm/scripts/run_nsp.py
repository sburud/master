import os
import re
from argparse import ArgumentParser
from collections.abc import Sized
from random import choice

import torch
import transformers
from torch.utils.data import DataLoader, Dataset
from torch.utils.data.dataset import T_co, ConcatDataset
from transformers import Trainer

from speechlm.env_config import MODELS_PATH, DATA_PATH, NO_CUDA
from speechlm.models import load_model, save_model

print(transformers.__version__)

parser = ArgumentParser()
parser.add_argument('--model-path', default='nb-bert-nsp-opensub')
parser.add_argument('--checkpoint', default='/checkpoint-79872')
parser.add_argument('--train-data', default='OpenSubtitles/nsp_train_no.lower_')
parser.add_argument('--val-data', default='OpenSubtitles/nsp_val_no.lower_')
parser.add_argument('--device-batch-size', type=int, default=128)
parser.add_argument('--eval', action='store_true')
parser.add_argument('--epochs', default=1, type=int)
args = parser.parse_args()
print(args)

MODEL_PATH = MODELS_PATH + args.model_path
CHECKPOINT = MODEL_PATH + args.checkpoint
TRAIN_DATA = DATA_PATH + args.train_data
VAL_DATA = DATA_PATH + args.val_data


class NspDataset(Dataset):

    def __init__(self, path: str, length: int = None) -> None:
        super().__init__()
        self._path = path
        self._data = None
        self._len = length

    def __getitem__(self, index) -> T_co:
        if self._data is None:
            self._data = torch.load(self._path)
        batch = self._data[index]
        if batch['input_ids'].shape[1] > 512:
            print('WARNING: Batch', index, 'of', self._path, 'will crash the model. Returning random batch instead')
            batch = choice(self._data)
        return batch

    def __len__(self):
        if self._data is None:
            if self._len is not None:
                return self._len
            self._data = torch.load(self._path)
        return len(self._data)


class ChunkedNspDataset(ConcatDataset, Sized):

    def __init__(self, path: str) -> None:
        super().__init__(self.get_nsp_datasets(path))

    @staticmethod
    def get_nsp_datasets(prefix: str):
        pieces = prefix.split('/')
        path = '/'.join(pieces[:-1])
        paths = [
            os.path.join(path, name)
            for name in sorted(os.listdir(path))
            if name.startswith(pieces[-1])
        ]
        regex = re.compile(prefix + '(\\d+)\\.pth')
        try:
            start_indices = [
                int(regex.match(path).group(1))
                for path in paths
            ]
        except:
            print('Loading all data in', paths)
            return [NspDataset(path) for path in paths]
        paths = list(sorted(paths, key=lambda path: start_indices[paths.index(path)]))
        start_indices.sort()
        datasets = []
        for i in range(len(paths)):
            path = paths[i]
            if i + 1 < len(paths):
                start_index, end_index = start_indices[i:i + 2]
                if start_index < end_index:
                    datasets.append(NspDataset(path, (end_index - start_index) // 32))
                else:
                    datasets.append(NspDataset(path))
            else:
                datasets.append(NspDataset(path))
        return datasets


if not args.eval:
    print('Load train')
    train = ChunkedNspDataset(TRAIN_DATA)
else:
    train = None

print('Load val')
val = ChunkedNspDataset(VAL_DATA)

tokenizer, model = load_model(args.model_path, 'nsp')


class CustomTrainer(Trainer):
    def get_dataloader(self, dataset):
        return DataLoader(dataset, batch_size=1, shuffle=False, collate_fn=lambda x: x[0], num_workers=0)

    # Possibly unstable interface, crash is probably due to wrong transformers version

    def get_train_dataloader(self): return self.get_dataloader(self.train_dataset)

    def get_eval_dataloader(self, eval_dataset):
        return self.get_dataloader(eval_dataset if eval_dataset is not None else self.eval_dataset)

    def get_test_dataloader(self, test_dataset):
        return self.get_dataloader(test_dataset if test_dataset is not None else self.test_dataset)


trainer = CustomTrainer(
    model=model,
    tokenizer=tokenizer,
    args=transformers.TrainingArguments(
        num_train_epochs=args.epochs,
        output_dir=MODEL_PATH,
        per_device_train_batch_size=args.device_batch_size,
        eval_steps=2048,
        save_steps=2048,
        gradient_accumulation_steps=2,
        no_cuda=NO_CUDA,
    ),
    train_dataset=train,
    eval_dataset=val,
    compute_metrics=print
)


def evaluate(model, dataset):
    correct, length = 0, 0
    confusion = torch.zeros((2, 2), dtype=torch.long, device=model.device)
    model.eval()
    with torch.no_grad():
        for i, batch in enumerate(dataset):
            labels = batch['labels'].to(model.device)
            batch = {key: value.to(model.device) for key, value in batch.items()}
            out = model.forward(**batch)
            preds = out.logits[:, 0] < out.logits[:, 1]
            correct += sum(preds == labels)
            for label, pred in zip(labels, preds):
                confusion[label, pred.long()] += 1
            length += len(labels)
            if i % 100 == 0 or i + 1 == len(dataset):
                print((correct / length).item(), confusion.diag() / confusion.sum(1), sep='\t\t')
    return (correct / length).item()


if __name__ == '__main__':
    if not args.eval:
        print(trainer.train(resume_from_checkpoint=CHECKPOINT))
        print('Saving…')
        save_model(args.model_path, tokenizer, model)
        print('Saved!')
    else:
        print('Skip train due to --eval flag')
    print('Final accuracy on validation data:', evaluate(model, val))
