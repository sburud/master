from run_nsp import *

def input_int(prompt):
    while True:
        try:
            return int(input(prompt))
        except Exception as e:
            print(e)

def evaluate_human(tokenizer, dataset):
    correct = 0
    length = 0
    for i, batch in enumerate(dataset):
        pred = torch.tensor([
                input_int(string.replace('[PAD]', '') + '\n')
                for string in tokenizer.batch_decode(batch['input_ids'])
        ], dtype=torch.long)
        labels = batch['labels']
        correct += sum(pred == labels) 
        length += len(labels)
        if i % 100 == 0 or True:
            print((correct / length).item())
    return (correct/length).item()

evaluate_human(tokenizer, val)
