from argparse import ArgumentParser
from dataclasses import dataclass
from itertools import chain
from multiprocessing.pool import Pool
from typing import List, Union

import optuna as optuna
import torch
from tqdm import tqdm

from decoder import BeamCTCDecoder
from speechlm.data.tnn import TranscribedTnn
from speechlm.env_config import NO_CUDA
from speechlm.scoring import word_edit_distance, load_scorer
from speechlm.scoring.scorer import Scorer
from speechlm.utils import floatrange

parser = ArgumentParser()

parser.add_argument('--dataset', type=str)
parser.add_argument('--transcript-type', type=str, default=None)
parser.add_argument('--scorer', type=str)
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('--debug', action='store_true')

manual = parser.add_argument_group(
    'Manually set params (running a trial will override these with values from the trial)')
manual.add_argument('--ngram-weight', type=float, default=1.0)
manual.add_argument('--length-weight', type=float, default=0.5)
manual.add_argument('--context-uttrs', type=int, default=2)
manual.add_argument('--ext-weight', type=float, default=0.5)
manual.add_argument('--beam-width', type=int, default=1024)
manual.add_argument('--num-groups', type=int, default=1)
manual.add_argument('--diversity-factor', type=float, default=0.1)

actions = parser.add_subparsers(dest='action', title='Subcommands')

optuna_parser = actions.add_parser('optuna')
optuna_parser.add_argument('-n', '--n-trials', type=int, default=1)
optuna_parser.add_argument('--study-name', type=str, default='dev')
optuna_parser.add_argument('-o', '--oracle', action='store_true', help='Use oracle rescoring instead of top beam')
optuna_parser.add_argument('--kenlm-tune', action='store_true')
optuna_parser.add_argument('--diversity', action='store_true')
optuna_parser.add_argument('--width-tune', action='store_true')

torch_parser = actions.add_parser('torch')

analyze_parser = actions.add_parser('analyze')

range_parser = actions.add_parser('range')
range_parser.add_argument('start', type=float, default=0.0)
range_parser.add_argument('end', type=float, default=1.0)
range_parser.add_argument('step', type=float, default=1e-2)


def calc_weds(target, preds):
    return [word_edit_distance(target, pred) for pred in preds]


def calc_batch_weds(targets, batch_preds):
    with Pool() as p:
        return p.starmap(calc_weds, ((target, preds) for target, preds in zip(targets, batch_preds)))


@dataclass
class OptimizationTask:
    decoder: BeamCTCDecoder
    ext_scorer: Scorer
    dataset: TranscribedTnn

    verbose: bool = False
    debug: bool = False
    oracle_mode: bool = False

    kenlm_tune: bool = False
    diversity_tune: bool = False
    width_tune: bool = False

    context_uttrs: int = 2
    ext_weight: float = 0.5

    scores: torch.Tensor = None
    targets: List[str] = None
    target_lengths: torch.Tensor = None
    targets_length: int = None
    strings: List[List[str]] = None
    weds: torch.Tensor = None

    def _wed2wer(self, weds, target_lengths: List[Union[int, str]] = None):
        if target_lengths is not None:
            if type(target_lengths[0]) is str:
                target_lengths = (len(s.split()) for s in target_lengths)
            return sum(weds) / sum(target_lengths)
        if type(weds) is list:
            return sum(weds) / self.targets_length
        else:
            return weds.sum() / self.targets_length

    def set_params(self, params: dict):
        if self.verbose:
            print('Set', params)
        if 'ngram_weight' in params or 'length_weight' in params:
            self.decoder._decoder.reset_params(params['ngram_weight'], params['length_weight'])
        if 'beam_width' in params:
            self.decoder.beam_width = params['beam_width']
        if 'num_groups' in params:
            self.decoder.num_groups = params['num_groups']
        if 'diversity_factor' in params:
            self.decoder.diversity_factor = params['diversity_factor']
        if 'ext_weight' in params:
            self.ext_weight = params['ext_weight']
        if 'context_uttrs' in params:
            self.context_utters = params['context_uttrs']

    def set_args_from_trial(self, trial):
        params = {}
        if self.kenlm_tune:
            params['ngram_weight'] = trial.suggest_float('ngram_weight', 0, 3)
            params['length_weight'] = trial.suggest_float('length_weight', 0, 2)

        if self.width_tune:
            params['beam_width'] = int(2 ** trial.suggest_int('beam_width_exp', 5, 10))

        if self.diversity_tune:
            params['num_groups'] = int(2 ** trial.suggest_int('num_groups_exp', 1, 9))
            if params['num_groups'] > 1:
                params['diversity_factor'] = 10 ** trial.suggest_int('diversity_factor_exp', -20, 10)

        if self.ext_scorer:
            params['ext_weight'] = trial.suggest_float('ext_weight', 0, 0.3)
        else:
            params['ext_weight'] = 0

        self.set_params(params)

    def optuna_objective(self, trial):
        self.set_args_from_trial(trial)

        trial_weds = []

        def callback(targets, strings, weds, scores, beam_scores, ext_scores, step):
            if self.oracle_mode:
                for i in range(len(weds)):
                    trial_weds.append(min(weds[i]))
            else:
                _, indices = torch.max(scores, dim=1)
                for i, j in enumerate(indices):
                    trial_weds.append(weds[i][j])

            trial.report(self._wed2wer(trial_weds, targets), step)
            if trial.should_prune():
                print('Pruned trial', trial.number, 'at step', step, 'and WER',
                      self._wed2wer(trial_weds))
                raise optuna.TrialPruned()

        self._run_scorers(callback)
        return self._wed2wer(trial_weds)

    def _torch_get_scores(self, params: torch.Tensor):
        scores = torch.matmul(torch.movedim(self.scores, 0, -1).cuda(), params)
        scores -= scores.min(1, keepdim=True)[0]
        maxed, indices = scores.max(1, keepdim=True)
        scores /= maxed
        return scores, indices[:, 0]

    def torch_optimize(self, max_stationary):
        self._run_scorers(None)

        n_samples = self.weds.shape[0]
        sample_range = torch.arange(n_samples)

        targets, best_indices = torch.min(self.weds, dim=1)
        truth = torch.zeros_like(self.weds, dtype=torch.float)
        truth[sample_range, best_indices] = 1

        print('Oracle disagrees with beam in', n_samples - torch.sum(truth[:, 0]).item(), 'cases')
        print('Best possible WER is', self._wed2wer(targets).item())
        print('Beam WER is', self._wed2wer(self.weds[:, 0]).item())

        # The numbers are often from different distributions, so normalize everything
        scores = torch.softmax(self.scores, dim=2)

        if not NO_CUDA:
            truth = truth.cuda()
            scores = scores.cuda()

        n_params = self.scores.shape[0]
        if n_params != 2:
            raise ValueError('Unsupported number of params: {}'.format(n_params))
        param = torch.tensor(self.ext_weight, dtype=torch.float, requires_grad=True, device=scores.device)
        opt = torch.optim.Adam([param], 1e-3)

        prev_loss = 1000
        loss = 999
        prev_wer = 0
        prev_param = None
        i = 0
        j = 0
        print('i', 'Loss', 'WED', 'WER', 'ext_weight', 'Beam disagreements', sep='\t')
        while loss < prev_loss or j < max_stationary:
            prev_loss = loss

            final_scores = scores[0] * (1 - param) + scores[1] * param

            max_scores, indices = torch.max(final_scores, dim=1)
            loss = torch.nn.functional.binary_cross_entropy(
                final_scores[sample_range, indices],
                truth[sample_range, indices]
            )
            # loss = -torch.sum(final_scores[sample_range, best_indices] / max_scores)
            loss.backward()
            opt.step()
            opt.zero_grad()

            weds = self.weds[sample_range, indices]
            wer = self._wed2wer(weds).item()
            wed = weds.sum().item()

            print(i, loss.item(), wed, wer, param.item(), torch.sum(indices != 0).item(), sep='\t')
            if wer < prev_wer:
                prev_param = param.item()
            prev_wer = wer
            i += 1
            if loss < prev_loss:
                j = 0
            else:
                j += 1
            if param < 0:
                print('ERROR:', param, 'must be >= 0')
                break
        print('Loss/WER after', i, 'optimization steps is', loss.item(), '/', wer)
        print('Final ext_weight is', param.item())
        if prev_param and param != prev_param:
            print('At last row with WER improvement, ext_weight was', prev_param)
        return param.detach()

    def calc_score_range(self, start=0, end=1, step=0.01):
        self._run_scorers(None)

        if NO_CUDA:
            weds = self.weds
            scores = self.scores
        else:
            weds = self.weds.cuda()
            scores = self.scores.cuda()

        n_samples = self.weds.shape[0]
        sample_range = torch.arange(n_samples)
        targets, best_indices = torch.min(weds, dim=1)
        best_line = []

        print('ext_score', 'WER', 'WED', '#incorrect', '#disagree', sep='\t')
        for ext_score in floatrange(start, end, step):
            final_scores = scores[0] * (1 - ext_score) + scores[1] * ext_score
            max_scores, indices = torch.max(final_scores, dim=1)
            weds_ = weds[sample_range, indices]
            incorrects = (best_indices != indices).sum().item()
            disagrees = (indices > 0).sum().item()
            wer = self._wed2wer(weds_).item()
            wed = weds_.sum().item()
            line = [ext_score, wer, wed, incorrects, disagrees]
            print(*line, sep='\t')
            if not best_line or best_line[2] > wed:
                best_line = line
        print()
        print('Best line:')
        print(*best_line, sep='\t')
        return best_line[0]

    def _add_contexts(self, infos, context_uttrs, strings):
        if context_uttrs > 0:
            contexts = [
                ' '.join(info['context'][-context_uttrs:])
                for info in infos
            ]
        else:
            contexts = [''] * len(infos)
        return [
            [(context, pred) for pred in preds]
            for preds, context in zip(strings, contexts)
        ]

    def _score_batch(self, strings, scorer):
        scores = scorer.score_docs(list(chain(*strings)))
        if type(scores) != torch.Tensor:
            scores = torch.tensor(scores)
        return scores.reshape((len(strings), len(strings[0])))

    def _am_decode(self, am_probs):
        beams, beam_scores_approx, _, lengths = self.decoder.raw_decode(am_probs)
        strings = self.decoder.convert_to_strings(beams, lengths)
        # beam_scores = 1/torch.exp(beam_scores_approx)  # From the docs, gives infinity in practice
        # beam_scores = -beam_scores_approx
        # beam_scores = beam_scores_approx
        beam_scores = torch.nn.functional.softmax(-beam_scores_approx, dim=1)
        return strings, beam_scores, lengths

    def _run_scorers(self, callback=None, keep_context=False):
        self.scores = torch.zeros(
            (2, len(self.dataset) * self.dataset.batch_size, self.decoder.beam_width),
            requires_grad=False
        )
        self.targets = []
        self.strings = []
        self.weds = []
        for step, batch in enumerate(tqdm(self.dataset)):
            infos = batch['info']
            offset = step * self.dataset.batch_size
            offset_end = offset + len(infos)
            self.decoder.beam_width = self.decoder.beam_width
            strings, beam_scores, lengths = self._am_decode(batch['am_probs'])

            self.scores[0, offset:offset_end, :] = beam_scores

            if self.ext_weight > 0:
                ext_scores = self._score_batch(self._add_contexts(infos, self.context_uttrs, strings), self.ext_scorer)
                self.scores[1, offset:offset_end, :] = ext_scores
            else:
                ext_scores = None

            targets = [infos[i][self.dataset.type.transcript_name] for i in range(len(infos))]
            self.targets += targets
            self.strings += strings
            weds = calc_batch_weds(targets, strings)
            self.weds += weds

            if callback:
                if self.ext_weight > 0:  # If ext_weight is 0, ext_scores doesn't exist
                    scores = (beam_scores * (1 - self.ext_weight)) + (ext_scores * self.ext_weight)
                else:
                    scores = beam_scores
                if keep_context:
                    contexts = [infos[i]['context'] for i in range(len(infos))]
                    callback(targets, strings, weds, scores, beam_scores, ext_scores, step, contexts)
                else:
                    callback(targets, strings, weds, scores, beam_scores, ext_scores, step)

            if self.debug: break

        self.scores = self.scores[:, :len(self.strings)]
        self.weds = torch.tensor(self.weds)
        self.target_lengths = torch.tensor([len(target.split()) for target in self.targets])
        self.targets_length = self.target_lengths.sum().item()

    def analyze(self):
        chosen_weds = []
        best_weds = []
        beam_weds = []

        def callback(targets, strings, weds, scores, beam_scores, ext_scores, step, contexts):
            _, indices = torch.max(scores, dim=1)
            for i, (target, preds, context) in enumerate(zip(targets, strings, contexts)):
                j = indices[i].item()
                pred = preds[j]
                chosen_weds.append(weds[i][j])
                best_weds.append(min(weds[i]))
                beam_weds.append(weds[i][0])
                #if 0.01 < min(weds[i]) < weds[i][j]:  # BERT could have done better
                if self.verbose or weds[i][j] < weds[i][0]:  # BERT did better than Beam
                    print(' | '.join(context[-self.context_uttrs:]), '[context]')
                    print(target, '[target]')
                    print(min(preds, key=lambda string: word_edit_distance(target, string)), '[WED best beam]')
                    if self.ext_weight > 0:
                        print(pred, '[Combined chosen beam]')
                        print(preds[torch.max(ext_scores[i, :], dim=0)[1]], '[Ext scorer chosen beam]')
                    print(preds[0], '[Beam search chosen beam]')
                    print(max(preds, key=lambda string: word_edit_distance(target, string)), '[WED worst beam]')

        self._run_scorers(callback, keep_context=True)
        print(list(map(self._wed2wer, (chosen_weds, beam_weds, best_weds))))


def main():
    args = parser.parse_args()
    print(args)
    tnn = TranscribedTnn(args.dataset, args.transcript_type)
    print(tnn.transcriber_config)
    scorer = load_scorer(args.scorer, False) if args.scorer is not None else None
    decoder = tnn.get_decoder()
    task = OptimizationTask(decoder, scorer, tnn, verbose=args.verbose, debug=args.debug)

    task.set_params(vars(args))

    if args.action == 'optuna':
        task.kenlm_tune = args.kenlm_tune
        task.diversity_tune = args.diversity
        task.width_tune = args.width_tune
        task.oracle_mode = args.oracle
        study = optuna.create_study(study_name=args.study_name, direction='minimize',
                                    # pruner=optuna.pruners.SuccessiveHalvingPruner(),
                                    storage='sqlite:///study.db', load_if_exists=True)
        if args.n_trials > 0:
            study.optimize(task.optuna_objective, n_trials=args.n_trials)

        if args.verbose:
            for trial in study.best_trials:
                print(trial)
        print('Best params are:', study.best_params)

        if args.n_trials == 0:
            print('Running best params in verbose mode')
            task.verbose = True
            print(task.optuna_objective(study.best_trial))
    elif args.action == 'torch':
        task.torch_optimize(5)
    elif args.action == 'analyze':
        task.analyze()
    elif args.action == 'range':
        task.calc_score_range(args.start, args.end, args.step)


if __name__ == '__main__':
    main()
