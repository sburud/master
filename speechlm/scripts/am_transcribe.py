import argparse
import multiprocessing
import os

import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from data.data_loader import SpectrogramDataset
from decoder import GreedyDecoder
from opts import add_decoder_args, add_inference_args
from utils import load_model

"""
Save transcriptions and DeepSpeech probs as dictionaries, mappable back to the original audio chunks.
"""

parser = argparse.ArgumentParser(description='DeepSpeech transcription')
parser = add_inference_args(parser)
parser.add_argument('--test-manifest', metavar='DIR',
                    help='path to validation manifest csv', default='data/test_manifest.csv')
parser.add_argument('--output-manifest', metavar='DIR', help='Place to save predicted transcriptions')
parser.add_argument('--batch-size', default=20, type=int, help='Batch size for testing')
parser.add_argument('--num-workers', default=4, type=int, help='Number of workers used in dataloading')
parser.add_argument('--verbose', action="store_true", help="print out decoded output and error of each sample")
parser.add_argument('--save-path', type=str, help="Saves output of model from test to this file_path")
parser.add_argument('--transcript-csv', type=str, help='A table with additional info to attach with the output metadata.')
parser = add_decoder_args(parser)


def _collate_fn(batch):
    def func(p):
        return p[0].size(1)

    indices = sorted(range(len(batch)), key=lambda i: func(batch[i]), reverse=True)
    batch = sorted(batch, key=func, reverse=True)
    longest_sample = max(batch, key=func)[0]
    freq_size = longest_sample.size(0)
    minibatch_size = len(batch)
    max_seqlength = longest_sample.size(1)
    inputs = torch.zeros(minibatch_size, 1, freq_size, max_seqlength)
    input_percentages = torch.FloatTensor(minibatch_size)
    target_sizes = torch.IntTensor(minibatch_size)
    targets = []
    for x in range(minibatch_size):
        sample = batch[x]
        tensor = sample[0]
        target = sample[1]
        seq_length = tensor.size(1)
        inputs[x][0].narrow(1, 0, seq_length).copy_(tensor)
        input_percentages[x] = seq_length / float(max_seqlength)
        target_sizes[x] = len(target)
        targets.extend(target)
    targets = torch.IntTensor(targets)
    return inputs, targets, input_percentages, target_sizes, indices


def evaluate(test_loader, device, model, decoder, save_path):
    model.eval()
    for i, (data) in tqdm(enumerate(test_loader), total=len(test_loader)):
        inputs, targets, input_percentages, target_sizes, indices = data
        input_sizes = input_percentages.mul_(int(inputs.size(3))).int()
        inputs = inputs.to(device)

        split_targets = []
        offset = 0
        for size in target_sizes:
            split_targets.append(targets[offset:offset + size])
            offset += size

        out, output_sizes = model(inputs, input_sizes)
        out = out.cpu()
        output_sizes = output_sizes.cpu()
        # strings, time_offsets, paths, scores = decoder.decode(out, output_sizes, return_paths=True)
        paths, _, _, lengths = decoder.raw_decode(out, output_sizes)
        # scores = decoder.calculate_beam_scores(out, paths, lengths)
        # strings = decoder.convert_to_strings(paths, lengths)

        torch.save({
            # 'strings': strings,
            'targets': split_targets,
            'beam_paths': paths,
            'beam_path_length': lengths,
            # 'am_scores': scores,
            'am_probs': out,
            'indices': indices,
        }, '{0}/{1}.pth'.format(save_path, str(i)))
    return i + 1


def init():
    global decoder
    from decoder import BeamCTCDecoder
    decoder = BeamCTCDecoder(model.labels, lm_path=None, alpha=args.alpha, beta=args.beta,
                             cutoff_top_n=args.cutoff_top_n, cutoff_prob=args.cutoff_prob,
                             beam_width=args.beam_width, num_processes=args.lm_workers)


def postprocessing(path):
    print('Load', path, sep='\t')
    data = torch.load(path)
    am_probs = data['am_probs']
    beam_paths = data['beam_paths']
    beam_path_lengths = data['beam_path_length']
    # print('Scores', path, sep='\t')
    data['am_scores'] = decoder.calculate_beam_scores(am_probs, beam_paths, beam_path_lengths)
    # print('Strings', path, sep='\t')
    data['strings'] = decoder.convert_to_strings(beam_paths, beam_path_lengths)
    print('Save', path, sep='\t')
    torch.save(data, path)
    # print('Done', path, sep='\t')


if __name__ == '__main__':
    args = parser.parse_args()
    print(args)
    if os.path.exists(args.save_path):
        print('WARNING: Will overwrite', args.save_path)
    else:
        os.mkdir(args.save_path)
    torch.set_grad_enabled(False)
    device = torch.device("cuda" if args.cuda else "cpu")
    model = load_model(device, args.model_path, args.half)
    if args.verbose:
        print(model)

    if args.decoder == "beam":
        from decoder import BeamCTCDecoder

        decoder = BeamCTCDecoder(model.labels, lm_path=args.lm_path, alpha=args.alpha, beta=args.beta,
                                 cutoff_top_n=args.cutoff_top_n, cutoff_prob=args.cutoff_prob,
                                 beam_width=args.beam_width, num_processes=args.lm_workers)
    elif args.decoder == "greedy":
        decoder = GreedyDecoder(model.labels, blank_index=model.labels.index('_'))
    else:
        decoder = None
    test_dataset = SpectrogramDataset(audio_conf=model.audio_conf, manifest_filepath=args.test_manifest,
                                      labels=model.labels, normalize=True)
    test_loader = DataLoader(test_dataset, batch_size=args.batch_size,
                             num_workers=1, collate_fn=_collate_fn)
    vars(args)['labels'] = model.labels
    torch.save(args, args.save_path + '/metadata.pth')
    n_batches = evaluate(test_loader=test_loader,
                         device=device,
                         model=model,
                         decoder=decoder,
                         save_path=args.save_path)

    tasks = [
        '{0}/{1}.pth'.format(args.save_path, str(i))
        for i in range(n_batches)
    ]
    torch.set_num_threads(1)  # Workaround for https://github.com/pytorch/pytorch/issues/17199
    with multiprocessing.Pool(32, init) as pool:
        print('Launch pool')
        pool.map(postprocessing, tasks)
        print('Done!')
