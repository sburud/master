from argparse import ArgumentParser
from dataclasses import dataclass
from itertools import chain
from typing import List

import optuna as optuna
import torch
from optuna import Trial
from tqdm import tqdm

from decoder import BeamCTCDecoder
from speechlm.data.tnn import TranscribedTnn
from speechlm.scoring import load_scorer, word_error_rate
from speechlm.scoring.scorer import Scorer

parser = ArgumentParser()
parser.add_argument('--dataset', type=str)
parser.add_argument('--fast-scorer', type=str)
parser.add_argument('--slow-scorer', type=str)
parser.add_argument('-n', '--n-trials', type=int, default=1)
parser.add_argument('--study-name', type=str, default='dev')
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('--beam-width', type=int, default=2)

@dataclass
class ThreeStepDecoder:
    decoder: BeamCTCDecoder
    fast_scorer: Scorer
    slow_scorer: Scorer
    context_uttrs: int = 2

    @classmethod
    def mean(cls, lst: List):
        return sum(lst) / len(lst)

    def run_scorers(self, tnn, n=2 ** 5):
        scores = torch.zeros((4, len(tnn) * tnn.batch_size, n))
        targets = []
        all_strings = []
        for step, batch in enumerate(tqdm(tnn)):
            offset = step * tnn.batch_size
            offset_end = offset + len(batch['info'])
            self.decoder.beam_width = n
            strings, am_scores, lengths = self.am_decode(batch['am_probs'])

            scores[0, offset:offset_end, :] = am_scores

            if self.fast_scorer is not None:
                fast_scores = self.score_batch(self.add_contexts(batch['info'], 0, strings), self.fast_scorer)
                scores[1, offset:offset_end, :] = fast_scores

            if self.slow_scorer is not None:
                slow_scores = self.score_batch(self.add_contexts(batch['info'], self.context_uttrs, strings), self.slow_scorer)
                scores[2, offset:offset_end, :] = slow_scores

            scores[3, offset:offset_end, :] = lengths

            infos = batch['info']
            targets += (infos[i][tnn.type.transcript_name] for i in range(len(infos)))
            all_strings += strings
        self.scores = scores
        self.targets = targets
        self.strings = all_strings
        return scores

    def optimize(self, trial:Trial, scores=None, targets=None, strings=None, verbose=False):
        if scores is None: scores = self.scores
        if targets is None: targets = self.targets
        if strings is None: strings = self.strings

        n_params = self.scores.shape[0]
        params = torch.tensor([
            trial.suggest_float('param_' + str(i), -2, 2)
            for i in range(n_params)
        ], dtype=torch.float)

        final_scores = torch.matmul(torch.movedim(scores, 0,-1), params)
        _, indices = torch.max(final_scores, dim=1)
        wer = 0.0
        for i, preds in enumerate(strings):
            pred = preds[indices[i]]
            target = targets[i]
            wer += word_error_rate(target, pred)
        if verbose:
            for i in range(len(strings)):
                target = targets[i]
                print(target, '[target]')
                print(min(strings[i], key=lambda string: word_error_rate(target, string)), '[min WER]')
                print(strings[i][indices[i].item()], '[combined choice]')
                for param in range(n_params):
                    print(strings[i][torch.max(scores[param,i], dim=0)[1].item()], '[param '+str(param)+' choice]')
                print(max(strings[i], key=lambda string: word_error_rate(target, string)), '[max WER]')
        return wer / len(strings)


    def score_batch(self, strings, scorer):
        scores = scorer.score_docs(list(chain(*strings)))
        if type(scores) != torch.Tensor:
            scores = torch.tensor(scores)
        return scores.reshape((len(strings), len(strings[0])))

    def am_decode(self, am_probs):
        beams, am_scores_approx, _, lengths = self.decoder.raw_decode(am_probs)
        strings = self.decoder.convert_to_strings(beams, lengths)
        # am_scores = decoder.calculate_beam_scores(batch['am_probs'], paths, lengths)
        # am_scores = 1/torch.exp(am_scores_approx) # From the docs, gives infinity in practice
        # am_scores = torch.exp(-am_scores_approx) # Same as above
        # am_scores = -am_scores_approx
        am_scores = torch.nn.functional.softmax(-am_scores_approx, dim=1)
        return strings, am_scores, lengths

    def add_contexts(self, infos, context_uttrs, strings):
        if context_uttrs > 0:
            contexts = [
                ' '.join(info['context'][-context_uttrs:])
                for info in infos
            ]
        else:
            contexts = [''] * len(infos)
        return [
            [(context, pred) for pred in preds]
            for preds, context in zip(strings, contexts)
        ]


def main():
    args = parser.parse_args()
    print(args)
    tnn = TranscribedTnn(args.dataset)
    fast_scorer = load_scorer(args.fast_scorer, False) if args.fast_scorer else None
    slow_scorer = load_scorer(args.slow_scorer, True) if args.slow_scorer else None
    beam_decoder = tnn.get_decoder(use_lm=False)
    decoder = ThreeStepDecoder(beam_decoder, fast_scorer, slow_scorer)
    decoder.run_scorers(tnn, args.beam_width)
    study = optuna.create_study(study_name=args.study_name, direction='minimize',
                                pruner=optuna.pruners.SuccessiveHalvingPruner(),
                                storage='sqlite:///threestep.db', load_if_exists=True)
    if args.n_trials > 0:
        study.optimize(decoder.optimize, n_trials=args.n_trials)

    for trial in study.best_trials:
        print(trial)

    print(decoder.optimize(study.best_trial, verbose=True))

if __name__ == '__main__':
    main()

