import os

from transformers import AutoModel, AutoTokenizer, PreTrainedTokenizerBase, PreTrainedModel
from transformers.models.bert import BertForMaskedLM, BertForNextSentencePrediction
from transformers.models.electra import ElectraTokenizerFast, ElectraForPreTraining
from transformers.models.roberta import RobertaTokenizerFast, RobertaForMaskedLM, RobertaForSequenceClassification

from speechlm.env_config import MODELS_PATH


def save_model(id, tokenizer: PreTrainedTokenizerBase, model: PreTrainedModel):
    path = os.path.join(MODELS_PATH, id)
    if os.path.exists(path):
        print('Overwriting', path)
    tokenizer.save_pretrained(os.path.join(path, 'tokenizer'))
    model.save_pretrained(os.path.join(path, 'model'))


def load_model(id='roberta-oscar-3', type: str = None):
    if id == 'nb':
        id = 'NbAiLab/nb-bert-base'
    path = MODELS_PATH + id
    if os.path.exists(path):
        model = _load_model(path, type)
        tokenizer = load_tokenizer(path)
    else:
        model = _load_model(id, type)
        tokenizer = load_tokenizer(id)
    return tokenizer, model


def _load_model(id, type):
    model_path = id + '/model'
    if os.path.exists(model_path):
        id = model_path
    print('Load', id)
    if 'roberta' in id:
        if type == 'nsp':
            return RobertaForSequenceClassification.from_pretrained(id)
        else:
            return RobertaForMaskedLM.from_pretrained(id)
    elif 'electra' in id:
        return ElectraForPreTraining.from_pretrained(id)
    elif type == 'mlm':
        return BertForMaskedLM.from_pretrained(id)
    elif type == 'nsp':
        return BertForNextSentencePrediction.from_pretrained(id)
    else:
        print("Not sure what model class " + id + " is")
        return AutoModel.from_pretrained(id)


def load_tokenizer(id='bert-oscar'):
    tokenizer_path = id + '/tokenizer/'
    if os.path.exists(tokenizer_path):
        id = tokenizer_path
    print('Load', id)
    if 'electra' in id:
        return ElectraTokenizerFast(id, tokenizer_file=tokenizer_path + 'tokenizer.json')
    elif 'roberta' in id:
        return RobertaTokenizerFast.from_pretrained(id, max_len=512)
    elif '/' in id:
        return AutoTokenizer.from_pretrained(id)
    else:
        print('Not sure what tokenizer class ' + id + ' is')
        return AutoTokenizer.from_pretrained(id)
