import os
from argparse import Namespace
from typing import List, Dict

import pandas as pd
import torch
from tqdm import tqdm

from decoder import BeamCTCDecoder
from speechlm.env_config import DATA_PATH, MODELS_PATH
from speechlm.utils import maybe_int, nice_open


class TranscriptType:
    UNIFIED_NOHESIT = 'u+nh'
    UNIFIED = 'u'
    NOHESIT = 'nh'
    ORIGINAL = 'o'
    NPSC = 'npsc'

    def __init__(self, type):
        self._type = type

    @property
    def transcript_name(self):
        if self.UNIFIED == self._type:
            return 'current_transcription_uni'
        if self.NOHESIT == self._type:
            return 'nohesit_transcription'
        if self.UNIFIED_NOHESIT == self._type:
            return 'nohesit_transcription_uni'
        if self.ORIGINAL == self._type:
            return 'current_transcription'
        if self.NPSC == self._type:
            return 'sentence_text_normalized'

    @property
    def txt_name(self):
        if self.UNIFIED == self._type:
            return 'txt_filename_uni'
        if self.NOHESIT == self._type:
            return 'txt_filename_nohesit'
        if self.UNIFIED_NOHESIT == self._type:
            return 'txt_filename_nohesit_uni'
        if self.ORIGINAL == self._type:
            return 'txt_filename'
        if self.NPSC == self._type:
            return 'txt_filename'

    @property
    def conv_id(self):
        if self.NPSC == self._type:
            return 'session_id'
        return 'htrid'

    @property
    def seq_id(self):
        if self.NPSC == self._type:
            return 'sentence_id'
        return 'Sequence_index'

    @classmethod
    def from_ds2(cls, ds2):
        nohesit = 'nohesit' in ds2.test_manifest or 'nohesit' in ds2.model_path
        unified = 'uni' in ds2.test_manifest or 'uni' in ds2.model_path or '_U' in ds2.model_path
        npsc = 'npsc' in ds2.test_manifest.lower()
        if npsc: return TranscriptType(cls.NPSC)
        if unified and nohesit: return TranscriptType(cls.UNIFIED_NOHESIT)
        if unified: return TranscriptType(cls.UNIFIED)
        if nohesit: return TranscriptType(cls.NOHESIT)
        return cls.ORIGINAL

    def __repr__(self) -> str:
        return self._type


class TranscribedTnn:

    def __init__(self, root='telenor/pred_CONV_EVAL-model_20210208_NST2/', ttype: TranscriptType = None):
        if not os.path.exists(root):
            root = os.path.join(DATA_PATH, root)
        if type(ttype) is str:
            print('TranscriptType('+ttype+') given as string, won\'t check if it is valid')
            ttype = TranscriptType(ttype)

        self.paths = [
            os.path.join(root, name)
            for name in
            sorted(os.listdir(root), key=lambda name: maybe_int(name[:name.index('.')]))
            if name.endswith('.pth') and 'metadata' not in name
        ]

        self.transcriber_config = self.fix_paths(torch.load(os.path.join(root, 'metadata.pth')))
        if ttype:
            self.type = ttype
        else:
            self.type = TranscriptType.from_ds2(self.transcriber_config)
            print('Guessing that transcript type is', self.type)
        self.transcripts = self.get_dataset_transcripts(self.transcriber_config.transcript_csv)
        self.batch_size = self.transcriber_config.batch_size

    def get_dataset_transcripts(self, dataset_metafile):
        ds2_manifest = pd.read_csv(self.transcriber_config.test_manifest, names=['wav', 'txt'])
        dataset_metafile = pd.read_csv(dataset_metafile)
        # For easier debugging
        ds2_manifest['ds2_transcript'] = [nice_open(p).read() for p in ds2_manifest['txt']]
        return dataset_metafile.merge(ds2_manifest, left_on=self.type.txt_name, right_on='txt', how='left')

    def supply_context(self, item_idx, data):
        col_transcript = self.type.transcript_name

        # Locate the appropriate rows based on batch offset from top of document and intra-batch location
        indices = data['indices']
        offset = item_idx * self.transcriber_config.batch_size
        try:
            txts = [self.transcripts.at[offset + i, 'txt'] for i in indices]
        except KeyError as e:
            # Temporary, occational crashes have been observed here.
            print(item_idx, self.transcriber_config.batch_size, offset)
            print(self.transcripts.describe())
            raise e

        # Create info object by looking up the ID located earlier.
        # FIXME: This could probably be merged into the line above for efficiency.
        data['info'] = [
            self.transcripts.loc[self.transcripts['txt'] == txt].to_dict('records')[0]
            for txt in txts
        ]
        for sample in data['info']:
            conv_id = sample[self.type.conv_id]
            seq_index = sample[self.type.seq_id]
            context = self.transcripts.loc[
                (self.transcripts[self.type.conv_id] == conv_id) &
                (self.transcripts[self.type.seq_id] < seq_index)
                ][col_transcript]
            sample['context'] = list(context)
        return data

    def check_field_presence(self, fields: List[str] = None):
        if fields is None:
            fields = ['targets', 'beam_paths', 'beam_path_length', 'am_probs',
                      'am_scores', 'strings', 'indices', 'info']

        missing_fields = []
        for path in tqdm(self.paths):
            data = torch.load(path)
            for key in fields:
                if key not in data:
                    missing_fields.append((path, key))

        return missing_fields

    def get_decoder(self, is_probabilities=True, use_lm=True):
        """
        Loads the same decoder that was used to transcribed this dataset instance
        (assuming decoder files are not overwritten since then)
        """
        if 'labels' in self.transcriber_config:
            labels = self.transcriber_config.labels
        else:
            print('Labels not included in dataset, attempting to load them from the AM')
            labels = torch.load(self.transcriber_config.model_path, map_location=lambda storage, loc: storage)['labels']
        return BeamCTCDecoder(
            labels=labels,
            lm_path=self.transcriber_config.lm_path if use_lm else None,
            alpha=self.transcriber_config.alpha if use_lm else 0.0,
            beta=self.transcriber_config.beta,
            cutoff_top_n=self.transcriber_config.cutoff_top_n,
            cutoff_prob=self.transcriber_config.cutoff_prob,
            beam_width=self.transcriber_config.beam_width,
            num_processes=self.transcriber_config.lm_workers,
            is_probabilities=is_probabilities,
        )

    def __len__(self):
        return len(self.paths)

    def __getitem__(self, item) -> Dict:
        data = torch.load(self.paths[item])
        if self.transcripts is not None:
            data = self.supply_context(item, data)
        return data

    @staticmethod
    def fix_paths(config: Namespace):
        # Workaround for some special-case paths at our infrastructure. Should be no-op for everyone else.
        return config
        d = vars(config)
        for key in d:
            if type(d[key]) == str and key != 'model_path':
                d[key] = d[key] \
                    .replace('/s2t_torch/data/', DATA_PATH) \
                    .replace('/s2t_torch/models/', MODELS_PATH) \
                    .replace('/s2t_torch/npsc_lm_model/', MODELS_PATH + 'kenlm-npsc/')
            if type(d[key]) == Namespace:
                d[key] = TranscribedTnn.fix_paths(d[key])
        return config
