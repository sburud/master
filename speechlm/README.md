# "Neural Language Models in Speech Recognition"

This directory contains the code for Simen Burud's master thesis.
See the master thesis (at [NTNU Open](https://ntnuopen.ntnu.no/ntnu-xmlui/)) for details.

A quick guide:

0. Train an acoustic model following the instructions in ../README.md
1. Obtain a huggingface.co-compatible BERT-like model.
   We support BERT, ELECTRA and ROBERTA directly, and use the _model name_ to determine the type.
   You may need to adapt `models.py` for your particular setup.
2. For the disambiguation task, use `scripts/am_transcribe.py` to write acoustic model results to disk.
3. Generate a training set for the model by following the `NSP fine-tuning` notebook.
4. Train/fine-tune your model using `scripts/run_nsp.py`
5. Use `scripts/minimize_wer.py` for hyperparameter tuning and analysis.

To use the diverse beam search, enter the directory `ctc-decode` and 
follow the instructions there to compile and install the C++ module.
If the decoder crashes on startup, you may have multiple versions of `ctcdecode`.
Delete all of them and compile it again.
