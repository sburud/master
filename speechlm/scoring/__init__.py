import Levenshtein

from .caching_scorer import CachingScorer
from .dummy_scorer import DummyScorer
from .electra_scorer import ElectraScorer
from .kenlm_scorer import KenLmScorer
from .mlm_scorer import MlmScorer
from .nsp_scorer import NspScorer
from speechlm.models import load_model


def load_scorer(id, cache=False):
    if cache: return CachingScorer(load_scorer(id, False))
    print('Load scorer', id)
    if 'kenlm' in id:
        return KenLmScorer(id)
    if 'electra' in id:
        return ElectraScorer(*load_model(id))
    if 'dummy' in id:
        return DummyScorer()
    if 'nsp' in id:
        return NspScorer(*load_model(id, 'nsp'))
    return MlmScorer(*load_model(id))


def word_error_rate(s1, s2):
    """
    Computes the Word Error Rate, defined as the edit distance between the
    two provided sentences after tokenizing to words, and dividing by the
    number of words in the first sentence.
    Arguments:
        s1 (string): space-separated sentence
        s2 (string): space-separated sentence
    """

    # build mapping of words to integers
    b = set(s1.split() + s2.split())
    word2char = dict(zip(b, range(len(b))))

    # map the words to a char array (Levenshtein packages only accepts
    # strings)
    w1 = [chr(word2char[w]) for w in s1.split()]
    w2 = [chr(word2char[w]) for w in s2.split()]

    return Levenshtein.distance(''.join(w1), ''.join(w2)) / len(w1)


def word_edit_distance(s1, s2):
    """
    Computes the Word Edit Distance, defined as the edit distance between the
    two provided sentences after tokenizing to words.
    Arguments:
        s1 (string): space-separated sentence
        s2 (string): space-separated sentence
    """

    # build mapping of words to integers
    b = set(s1.split() + s2.split())
    word2char = dict(zip(b, range(len(b))))

    # map the words to a char array (Levenshtein packages only accepts
    # strings)
    w1 = [chr(word2char[w]) for w in s1.split()]
    w2 = [chr(word2char[w]) for w in s2.split()]

    return Levenshtein.distance(''.join(w1), ''.join(w2))
