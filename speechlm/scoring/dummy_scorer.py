from random import random
from typing import List, Tuple

from .scorer import Scorer


class DummyScorer(Scorer):
    def score_docs(self, docs: List[Tuple[str, str]]) -> List[float]:
        return [self.score_doc(doc) for doc in docs]

    def score_doc(self, doc: Tuple[str, str]) -> float:
        return random()

    def get_token_counts(self, docs: List[str]) -> List[int]:
        # Inefficient, but idiot-proof way of getting exact tokenization
        return [1 for doc in docs]


if __name__ == '__main__':
    scorer = DummyScorer()
    text = "Hello world"
    print(scorer.get_token_counts([text]))
    score = scorer.score_doc(text)
    print(score)
