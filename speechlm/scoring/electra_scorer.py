from typing import List, Tuple

import torch

from speechlm.scoring.scorer import HuggingfaceScorer
from speechlm.models import load_model


class ElectraScorer(HuggingfaceScorer):

    def score_docs(self, documents: List[Tuple[str,str]]) -> List[float]:
        docs = self.tokenizer(documents, return_tensors='pt', padding=True, truncation=True)
        n_docs, length = docs.input_ids.shape

        # Move tensors to correct GPU if needed
        docs.data = {key: docs.data[key].to(self.model.device) for key in docs.data}

        with torch.no_grad():
            logits = self.model(**docs.data, return_dict=True).logits.detach()

        # If we only give a single doc, huggingface will remove the first dimension
        if len(logits.shape) == 1:
            logits = logits.unsqueeze(0)

        # Remove probabilities of padding tokens
        logits *= docs.attention_mask
        return [
            (torch.sum(doc_logits)).item()
            for doc_logits in logits
        ]


if __name__ == '__main__':
    scorer = ElectraScorer(*load_model('electra-grc'))
    all_tokens = list(scorer.tokenizer.vocab.values())
    i = 60
    print(scorer.score_docs(['dette ost virkelig gode nyheter ', 'dette er virkelig gode nyheter ']))
