from abc import ABC, abstractmethod
from collections import namedtuple
from typing import List, Tuple

import torch as torch
from torch.nn import DataParallel
from transformers import PreTrainedTokenizerFast, PreTrainedModel

from speechlm.env_config import NO_CUDA

ConversationContext = namedtuple('ConversationContext', ['conv_range', 'uttr_range', 'context_range'])


class Scorer(ABC):
    @abstractmethod
    def score_docs(self, docs: List[Tuple[str, str]]) -> List[float]:
        """
        Score the given documents. The first item of each tuple should be the conversational context,
        while the second item should be the string to be scored.
        The function returns some sort of list/tensor/np.array with float probabilities.
        """

    def score_doc(self, doc: Tuple[str, str]) -> float:
        """
        Same as @see score_docs, but with a single document.
        """
        return self.score_docs([doc])[0]

    @abstractmethod
    def get_token_counts(self, docs: List[str]) -> List[int]:
        pass

    def score_conv(self, conversation: List[str], context_size: int) -> List[float]:
        docs = [
            (
                ' '.join(conversation[i - context_size:i]),
                conversation[i]
            )
            for i in range(len(conversation))
        ]
        return self.score_docs(docs)


class HuggingfaceScorer(Scorer, ABC):

    def __init__(self, tokenizer: PreTrainedTokenizerFast, model: PreTrainedModel) -> None:
        self.model = model.eval()
        if not NO_CUDA:
            self.model = self.model.to('cuda')
            if torch.cuda.device_count() > 1:
                self.model_dp = DataParallel(self.model)
            else:
                self.model_dp = None
        else:
            # For compat with CPU-only mode
            self.model_dp = None
        self.tokenizer = tokenizer

    def get_token_counts(self, docs: List[str]) -> List[int]:
        conv_ids = self.tokenizer.batch_encode_plus(docs).input_ids
        return [len(tokens) - 2 for tokens in conv_ids]
