from typing import List, Tuple

import torch
from transformers import PreTrainedTokenizerFast, PreTrainedModel

from speechlm.env_config import NO_CUDA
from speechlm.models import load_model
from speechlm.scoring.scorer import HuggingfaceScorer


def chunkify(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


class NspScorer(HuggingfaceScorer):

    def __init__(self, tokenizer: PreTrainedTokenizerFast, model: PreTrainedModel) -> None:
        super().__init__(tokenizer, model)
        self.sep = self.tokenizer.sep_token
        self.max_batch_size = 512
        if self.model_dp is not None:
            self.max_batch_size *= len(self.model_dp.device_ids)

    def score_docs(self, docs: List[Tuple[str, str]]):
        if len(docs) == 0: return []
        if len(docs) > self.max_batch_size:
            return torch.cat([self.score_docs(doc) for doc in chunkify(docs, self.max_batch_size)])
        docs = self.tokenizer(docs, return_tensors='pt',
                              padding='longest', truncation='only_first',
                              max_length=self.model.config.max_position_embeddings)
        if not NO_CUDA and not self.model_dp:
            docs.data = {key: docs.data[key].to(self.model.device) for key in docs.data}
        with torch.no_grad():
            model = self.model_dp or self.model
            probs = model(**docs.data, return_dict=True).logits.detach()
        if len(probs.shape) == 1:
            probs = probs.unsqueeze(0)
        return probs[:, 1].cpu()


if __name__ == '__main__':
    scorer = NspScorer(*load_model('nb-bert-nsp/opensub-noort-npsc-am', 'nsp'))
    all_tokens = list(scorer.tokenizer.vocab.values())
    i = 60
    print(torch.nn.functional.softmax(scorer.score_docs([
        ('dette ost', 'virkelig gode nyheter'),
        ('dette var', 'virkelig gode nyheter'),
        ('gå vekk', 'virkelig gode nyheter')
    ]), dim=0))
