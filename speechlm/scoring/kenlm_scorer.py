from typing import List, Tuple

import kenlm

from speechlm.env_config import MODELS_PATH
from speechlm.scoring.scorer import Scorer


class KenLmScorer(Scorer):
    def __init__(self, id):
        if type(id) == str:
            self.model = kenlm.Model(MODELS_PATH + id)
        else:
            self.model = id

    def score_docs(self, docs: List[Tuple[str, str]]) -> List[float]:
        return [self.score_doc(doc) for doc in docs]

    def score_doc(self, doc: Tuple[str, str]) -> float:
        ctx_length = self.get_token_count(doc[0])
        scores = [
            prob
            for prob, length, oov in
            list(self.model.full_scores(' '.join(doc), eos=False))[ctx_length:]
        ]
        return sum(scores)

    def get_token_counts(self, docs: List[str]) -> List[int]:
        return [self.get_token_count(doc) for doc in docs]

    def get_token_count(self, doc: str):
        # Inefficient, but idiot-proof way of getting exact tokenization
        scores = self.model.full_scores(doc, eos=False)
        return len(list(scores))


if __name__ == '__main__':
    scorer = KenLmScorer('kenlm-oscar/2-pruned.bin')
    text = "Hello world".split(' ')
    print(scorer.get_token_counts(text))
    score = scorer.score_doc(text)
    print(score)
