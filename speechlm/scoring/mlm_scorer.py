from random import choice
from typing import List, Tuple

import torch
from tqdm import trange
from transformers import BatchEncoding, PreTrainedTokenizerFast, PreTrainedModel

from speechlm import env_config
from speechlm.models import load_model
from speechlm.scoring.scorer import HuggingfaceScorer


class MlmScorer(HuggingfaceScorer):

    def __init__(self, tokenizer: PreTrainedTokenizerFast, model: PreTrainedModel, step_size=None) -> None:
        super().__init__(tokenizer, model)
        self.step_size = step_size
        self._is_testing = False

    def score_docs(self, docs: List[Tuple[str, str]], regions: List[Tuple[int, int]] = None) -> List[float]:
        docs = self.tokenizer(docs, return_tensors='pt', padding=True)
        n_docs, length = docs.input_ids.shape
        batch_size = 10 ** 5 // length ** 2
        if batch_size == 0:
            batch_size = 1

        if self.step_size is None:
            print('WARNING: taking a break to set find a usable step size.')
            self.set_step_size(min(batch_size, n_docs), length, 4096)

        results = []
        rng = range if n_docs / batch_size < 10 else trange
        for i in rng(0, n_docs, batch_size):
            batch = BatchEncoding(
                data={
                    key: value[i:i + batch_size].to(self.model.device)
                    for key, value in docs.data.items()
                },
                encoding=docs.encodings[i:i + batch_size])
            results += self._score_docs(batch, regions[i:i + batch_size] if regions is not None else None)
            if self._is_testing:
                torch.cuda.synchronize()
                break
        return results

    # noinspection PyUnresolvedReferences
    def score_doc(self, doc: Tuple[str, str]) -> float:
        doc = self.tokenizer.sep_token.join(doc)
        ids = self.tokenizer.encode(doc, return_tensors='pt')[0]
        start = list(ids).index(self.tokenizer.sep_token_id) + 1
        # print(ids)
        # print(self.tokenizer.convert_ids_to_tokens(ids))
        length = len(ids)

        # Replace diagonal with masks
        I = torch.eye(length, length, dtype=torch.long)
        ids_masked = ids.repeat(length, 1) * (1 - I) + I * self.tokenizer.mask_token_id

        # noinspection PyArgumentList
        all_probs = torch.log_softmax(self.model.forward(ids_masked, return_dict=True).logits.detach(), dim=2)
        # probs has shape (length, length, vocab_size), all we care about is the "diagonal".
        # The diagonal contains prob. of seeing each possible token as a replacement for the mask at that position.
        diag_probs = all_probs.diagonal()[ids, range(length)]
        diag_probs = diag_probs[1:-1]  # Ignore probability of SOS and EOS tokens.
        diag_probs = diag_probs[start:]
        # print((diag_probs))
        return (torch.sum(diag_probs)).item()

    def _score_docs(self, docs: BatchEncoding, regions: List[Tuple[int, int]] = None) -> List[float]:
        n_docs, length = docs.input_ids.shape
        # FIXME: This is very memory-inefficient for large context sizes, since we usually only care about region scores
        # Make sample_length copies of each sample, and replace diagonal for each sample with mask tokens
        ids_duplicated = docs.input_ids.repeat_interleave(length, dim=0)
        I = torch.eye(length, length, dtype=torch.long, device=self.model.device).repeat(n_docs, 1)
        ids_masked = ids_duplicated * (1 - I) + I * self.tokenizer.mask_token_id
        attention_mask = docs.attention_mask.repeat_interleave(length, dim=0)

        all_logits = torch.zeros(
            (ids_masked.shape[0], self.tokenizer.vocab_size),
            device=self.model.device
        )

        # Run through the model
        self.model.eval()
        with torch.no_grad():
            for i in range(0, ids_masked.shape[0], self.step_size):
                f = i
                t = i + self.step_size
                all_logits[f:t] = self._forward(attention_mask[f:t], ids_masked[f:t], I[f:t])
                if self._is_testing:
                    torch.cuda.synchronize()
                    break

        # all_logits has shape (length*n_docs, vocab_size), all we care about is the prob. of seeing each
        # actual token as a replacement for the mask at that position.
        logits = [
            self.extract_token_logits(doc_enc, doc_probs, length)
            for doc_probs, doc_enc in zip(all_logits.split(split_size=length, dim=0), docs.encodings)
        ]

        if regions is not None:
            return [
                (torch.sum(doc_logits[start:end])).item()
                for doc_logits, [start, end] in zip(logits, regions)
            ]
        else:
            return [
                (torch.sum(doc_logits)).item()
                for doc_logits in logits
            ]

    def _forward(self, attention_mask, input_ids, mask_positions):
        logits = self.model.forward(input_ids=input_ids, attention_mask=attention_mask)[0].detach()
        logits = logits[mask_positions == 1]
        logits = torch.log_softmax(logits, dim=1)
        return logits

    @staticmethod
    def extract_token_logits(doc_enc, doc_logits, length):
        """
        doc_logits has shape (length, vocab_size), for each position we want the
        logit corresponding to the original token
        """
        diag_logits = doc_logits[range(length), doc_enc.ids]
        # Ignore special tokens
        diag_logits = diag_logits.masked_select(torch.BoolTensor([1 - it for it in doc_enc.special_tokens_mask]))
        #  print((diag_logits))
        return diag_logits

    def set_step_size(self, n_docs, length, step_size=None):
        if step_size is not None:
            self.step_size = step_size
        if self.step_size is None:
            self.step_size = 2 ** 15  # Start with an insane value
        all_tokens = list(self.tokenizer.vocab.values())
        garbage = [
            self.tokenizer.decode([choice(all_tokens) for _ in range(length)])
            for _ in range(n_docs)
        ]
        self._is_testing = True
        while True:
            try:
                print('Try step size', self.step_size)
                self.score_docs(garbage)
                print('Will use step size', self.step_size)
                self._is_testing = False
                return self.step_size
            except Exception as e:
                if 'CUDA' not in str(e): raise e
                self.step_size //= 2
                if self.step_size < 1:
                    self.step_size = None
                    self._is_testing = False
                    raise e


if __name__ == '__main__':
    if env_config.NO_CUDA:
        scorer = MlmScorer(*load_model('roberta-oscar-3'), step_size=1)
        print(scorer.score_docs([('kontekst', 'er viktig'), ('kontekst', 'ost',)]))
    else:
        scorer = MlmScorer(*load_model('roberta-oscar-3'), step_size=4096)
        print('Final step size is', scorer.set_step_size(74, 85))
