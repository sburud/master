from typing import List, Tuple

import torch

from speechlm.scoring.scorer import Scorer


class CachingScorer(Scorer):
    def __init__(self, scorer:Scorer):
        self.scorer = scorer
        self.cache = dict()

    def score_docs(self, docs: List[Tuple[str, str]]) -> List[float]:
        uncached = list(set((
            doc
            for doc in docs
            if doc not in self.cache
        )))
        scores = self.scorer.score_docs(uncached)
        for doc, score in zip(uncached, scores):
            self.cache[doc] = score
        return torch.tensor([self.cache[doc] for doc in docs])

    def get_token_counts(self, docs: List[str]) -> List[int]:
        return self.scorer.get_token_counts(docs)

    def score_conv(self, conversation: List[str], context_size: int) -> List[float]:
        return super().score_conv(conversation, context_size)
