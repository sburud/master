from speechlm.env_config import DATA_PATH


def nice_open(path):
    try:
        return open(path)
    except FileNotFoundError:
        return open(path.replace('/s2t_torch/data/', DATA_PATH))


def read_file_string(path):
    with nice_open(path) as f:
        return f.read()


def maybe_int(string: str):
    try:
        return int(string)
    except ValueError:
        return -1


def to_chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def floatrange(start, end, step):
    i = 0
    value = start
    while value < end:
        yield value
        i += 1
        value = start + step * i

