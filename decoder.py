#!/usr/bin/env python
# ----------------------------------------------------------------------------
# Copyright 2015-2016 Nervana Systems Inc.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ----------------------------------------------------------------------------
# Modified to support pytorch Tensors

import Levenshtein as Lev
import torch


class Decoder(object):
    """
    Basic decoder class from which all other decoders inherit. Implements several
    helper functions. Subclasses should implement the decode() method.

    Arguments:
        labels (list): mapping from integers to characters.
        blank_index (int, optional): index for the blank '_' character. Defaults to 0.
    """

    def __init__(self, labels, blank_index=0):
        self.labels = labels
        self.int_to_char = dict([(i, c) for (i, c) in enumerate(labels)])
        self.blank_index = blank_index
        space_index = len(labels)  # To prevent errors in decode, we add an out of bounds index for the space
        if ' ' in labels:
            space_index = labels.index(' ')
        self.space_index = space_index

    def wer(self, s1, s2):
        """
        Computes the Word Error Rate, defined as the edit distance between the
        two provided sentences after tokenizing to words.
        Arguments:
            s1 (string): space-separated sentence
            s2 (string): space-separated sentence
        """

        # build mapping of words to integers
        b = set(s1.split() + s2.split())
        word2char = dict(zip(b, range(len(b))))

        # map the words to a char array (Levenshtein packages only accepts
        # strings)
        w1 = [chr(word2char[w]) for w in s1.split()]
        w2 = [chr(word2char[w]) for w in s2.split()]

        return Lev.distance(''.join(w1), ''.join(w2))

    def cer(self, s1, s2):
        """
        Computes the Character Error Rate, defined as the edit distance.

        Arguments:
            s1 (string): space-separated sentence
            s2 (string): space-separated sentence
        """
        s1, s2, = s1.replace(' ', ''), s2.replace(' ', '')
        return Lev.distance(s1, s2)

    def convert_to_strings(self, indices, sizes):
        """
        Given a list of lists of character indices (or just a tensor), returns the corresponding strings.

        Args:
            indices: shape(batch_size, <=sequence_length).
            sizes: max length of each sequence
        """
        raise NotImplementedError

    def decode(self, probs, sizes=None, return_paths=False):
        """
        Given a matrix of character probabilities, returns the decoder's
        best guess of the transcription

        Arguments:
            probs: Tensor of character probabilities, where probs[c,t]
                            is the probability of character c at time t
            sizes(optional): Size of each sequence in the mini-batch
            return_paths: If true, return a third implementation-specific value containing
                            the paths taken through the probs matrix.
        Returns:
            string: sequence of the model's best guess for the transcription
        """
        raise NotImplementedError


class BeamCTCDecoder(Decoder):
    def __init__(self, labels, lm_path=None, alpha=0, beta=0, cutoff_top_n=40, cutoff_prob=1.0, beam_width=100,
                 num_processes=4, num_groups=1, diversity_factor=0.01, blank_index=0, is_probabilities=True):
        super(BeamCTCDecoder, self).__init__(labels)
        try:
            from ctcdecode import CTCBeamDecoder
        except ImportError:
            raise ImportError("BeamCTCDecoder requires ctcdecode package.")
        labels = list(labels)  # Ensure labels are a list before passing to decoder
        self._decoder = CTCBeamDecoder(
            labels=labels,
            model_path=lm_path,
            alpha=alpha,
            beta=beta,
            cutoff_top_n=cutoff_top_n,
            cutoff_prob=cutoff_prob,
            beam_width=beam_width,
            num_processes=num_processes,
            num_groups=num_groups,
            diversity_factor=diversity_factor,
            blank_id=blank_index,
        )
        self._decoder._log_probs = 0 if is_probabilities else 1
        self.raw_decode = self._decoder.decode

    @property
    def beam_width(self):
        return self._decoder._beam_width

    @beam_width.setter
    def beam_width(self, value):
        self._decoder._beam_width = value

    @property
    def num_groups(self):
        return self._decoder._num_groups

    @num_groups.setter
    def num_groups(self, value):
        self._decoder._num_groups = value

    @property
    def diversity_factor(self):
        return self._decoder._diversity_factor

    @diversity_factor.setter
    def diversity_factor(self, value):
        self._decoder._diversity_factor = value

    def convert_to_strings(self, indices, sizes):
        results = []
        for b, batch in enumerate(indices):
            utterances = []
            for p, utt in enumerate(batch):
                size = sizes[b][p]
                if size > 0:
                    transcript = ''.join(map(lambda x: self.int_to_char[x.item()], utt[0:size]))
                else:
                    transcript = ''
                utterances.append(transcript)
            results.append(utterances)
        return results

    @staticmethod
    def _tensor_to_lists(offsets, sizes):
        results = []
        for b, batch in enumerate(offsets):
            utterances = []
            for p, utt in enumerate(batch):
                size = sizes[b][p]
                if sizes[b][p] > 0:
                    utterances.append(utt[0:size])
                else:
                    utterances.append(torch.tensor([], dtype=torch.int))
            results.append(utterances)
        return results

    def decode(self, probs, sizes=None, return_paths=False):
        """
        Decodes probability output using ctcdecode package.
        Arguments:
            probs: Tensor of character probabilities, where probs[c,t]
                            is the probability of character c at time t
            sizes: Size of each sequence in the mini-batch
            return_paths: If true, return a third implementation-specific value containing
                            the paths taken through the probs matrix.
        Returns:
            string: sequences of the model's best guess for the transcription
        """
        probs = probs.cpu()
        paths, _, offsets, seq_lens = self._decoder.decode(probs, sizes)

        strings = self.convert_to_strings(paths, seq_lens)
        offsets = self._tensor_to_lists(offsets, seq_lens)

        if return_paths:
            return strings, offsets, paths
        else:
            return strings, offsets

    def calculate_beam_scores(self, probs, path_indices, path_lengths):
        """
        Calculates the AM's probability for each of the given paths ("strings").

        Args:
            probs: Tensor of character probabilities
            path_indices: Long tensor of shape (n_audio_clips, n_beams, time)
            path_lengths: End time for each beam, shape (n_audio_clips, n_beams)

        Returns:
            tensor.float: log-probs shape (n_audio_clips, n_beams)
        """
        scores = torch.zeros(path_lengths.shape)
        path_indices = torch.as_tensor(path_indices, dtype=torch.long)
        for batch, (sample_probs, indices, lengths) in enumerate(zip(probs, path_indices, path_lengths)):
            for beam, (index, length) in enumerate(zip(indices, lengths)):
                path_probs = torch.take(sample_probs, index[:length])
                # noinspection PyTypeChecker
                path_probs = torch.masked_select(path_probs, index[:length] != self.blank_index)
                scores[batch, beam] = torch.sum(torch.log(path_probs))
        return scores


class GreedyDecoder(Decoder):
    def __init__(self, labels, blank_index=0):
        super(GreedyDecoder, self).__init__(labels, blank_index)

    def convert_to_strings(self, sequences, sizes=None, remove_repetitions=False, return_offsets=False):
        """Given a list of numeric sequences, returns the corresponding strings"""
        strings = []
        offsets = [] if return_offsets else None
        for x in range(len(sequences)):
            seq_len = sizes[x] if sizes is not None else len(sequences[x])
            string, string_offsets = self.process_string(sequences[x], seq_len, remove_repetitions)
            strings.append([string])  # We only return one path
            if return_offsets:
                offsets.append([string_offsets])
        if return_offsets:
            return strings, offsets
        else:
            return strings

    def process_string(self, sequence, size, remove_repetitions=False):
        string = ''
        offsets = []
        for i in range(size):
            char = self.int_to_char[sequence[i].item()]
            if char != self.int_to_char[self.blank_index]:
                # if this char is a repetition and remove_repetitions=true, then skip
                if remove_repetitions and i != 0 and char == self.int_to_char[sequence[i - 1].item()]:
                    pass
                elif char == self.labels[self.space_index]:
                    string += ' '
                    offsets.append(i)
                else:
                    string = string + char
                    offsets.append(i)
        return string, torch.tensor(offsets, dtype=torch.int)

    def decode(self, probs, sizes=None, return_paths=False):
        """
        Returns the argmax decoding given the probability matrix. Removes
        repeated elements in the sequence, as well as blanks.

        Arguments:
            probs: Tensor of character probabilities from the network. Expected shape of batch x seq_length x output_dim
            sizes(optional): Size of each sequence in the mini-batch
            return_paths: If true, return a third implementation-specific value containing
                            the paths taken through the probs matrix.
        Returns:
            strings: sequences of the model's best guess for the transcription on inputs
            offsets: time step per character predicted
        """
        max_probs, max_indices = torch.max(probs, dim=2)
        strings, offsets = self.convert_to_strings(max_indices, sizes, remove_repetitions=True, return_offsets=True)
        # string_probs = torch.mean(max_probs, dim=1)
        if return_paths:
            return strings, offsets, max_probs
        else:
            return strings, offsets
